# delft-icon-theme

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0)

Continuation of Faenza icon theme with up to date app icons

https://github.com/madmaxms/iconpack-delft/

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/icons-and-themes/delft-icon-theme.git
```
<br><br>
<p align="center">
<img src="https://gitlab.com/azul4/content/icons-and-themes/delft-icon-theme/-/raw/main/delft-icon-theme-set.jpg">
</p>


